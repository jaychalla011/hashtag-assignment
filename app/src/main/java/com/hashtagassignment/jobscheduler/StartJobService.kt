package com.hashtagassignment.jobscheduler

import android.app.job.JobParameters
import android.app.job.JobService
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.hashtagassignment.activity.HomeActivity


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class StartJobService : JobService(){
    override fun onStopJob(params: JobParameters?): Boolean {
        onStartJob(params)
        return false
    }

    override fun onStartJob(params: JobParameters?): Boolean {
        getDataAndStoreLocal()
        return true
    }

    private fun getDataAndStoreLocal() {
        HomeActivity().updateUI()
    }
}