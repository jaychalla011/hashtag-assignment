package com.hashtagassignment.jobscheduler

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi

class Utils {
    private val TAG =
        Utils::class.java.simpleName

    /**
     * schedule job once
     *
     * @param context
     * @param cls
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun scheduleJob(context: Context, cls: Class<*>?) {
        val serviceComponent = ComponentName(context, cls!!)
        val builder = JobInfo.Builder(0, serviceComponent)
        builder.setMinimumLatency(1 * 1000.toLong()) // wait at least
        builder.setOverrideDeadline(5 * 1000.toLong()) // maximum delay
        builder.setRequiresCharging(false) // we don't care if the device is charging or not
        val jobScheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        jobScheduler.schedule(builder.build())
    }
}