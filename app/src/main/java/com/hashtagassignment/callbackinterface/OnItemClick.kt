package com.hashtagassignment.callbackinterface

interface OnItemClick {
    fun itemClick(int: Int, any: Any)
}