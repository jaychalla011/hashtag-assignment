package com.hashtagassignment.room.entity

import androidx.room.*
import com.hashtagassignment.room.typeconverter.Conveter

@Entity(tableName = "HashTag")
class HashTag {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id : Int = 0

    @TypeConverters(Conveter::class)
    var routeTimings: RouteTimings? = null

    @TypeConverters(Conveter::class)
    var routeInfo: List<RouteInfo>?= null

}