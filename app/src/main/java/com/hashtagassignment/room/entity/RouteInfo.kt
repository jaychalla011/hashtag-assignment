package com.hashtagassignment.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "RouteInfo")
class RouteInfo {
    @ColumnInfo(name = "id")
    var id: String? = null

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "source")
    var source: String? = null

    @ColumnInfo(name = "tripDuration")
    var tripDuration: String? = null

    @ColumnInfo(name = "destination")
    var destination: String? = null

    @ColumnInfo(name = "icon")
    var icon: String? = null
}
