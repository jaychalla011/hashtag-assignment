package com.hashtagassignment.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "RouteTimings")
class RouteTimings {
	@ColumnInfo(name = "route")
	var route : Map<String,List<RouteTimingSub>>? =null
}
