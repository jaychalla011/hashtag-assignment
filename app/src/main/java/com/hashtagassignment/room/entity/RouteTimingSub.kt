package com.hashtagassignment.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "RouteTimingSub")
class RouteTimingSub {

    @ColumnInfo(name = "totalSeats")
    var totalSeats: String? = null

    @ColumnInfo(name = "avaiable")
    var avaiable: String? = null

    @ColumnInfo(name = "tripStartTime")
    var tripStartTime: String? = null

}