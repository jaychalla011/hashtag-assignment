package com.hashtagassignment.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.hashtagassignment.room.entity.HashTag

@Dao
interface HashTagDao {
    @Query("Select * From HashTag Limit 1")
    fun getHashTag(): HashTag

    @Insert
    fun insertHashTag(vararg hashTag: HashTag)

}