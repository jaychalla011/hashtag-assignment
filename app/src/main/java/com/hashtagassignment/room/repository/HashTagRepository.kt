package com.hashtagassignment.room.repository

import com.hashtagassignment.room.entity.HashTag

class HashTagRepository(var iHashTagRepository: IHashTagRepository) : IHashTagRepository {
    override fun getHashTag(): HashTag {
        return iHashTagRepository.getHashTag()
    }

    override fun saveData(vararg data: HashTag) {
        iHashTagRepository.saveData(*data)
    }

    companion object {
        private var instance: HashTagRepository? = null

        fun getInstance(iHashTagRepository: IHashTagRepository): HashTagRepository {
            if (instance == null) {
                instance = HashTagRepository(iHashTagRepository)
            }
            return instance as HashTagRepository
        }
    }
}