package com.hashtagassignment.room.repository

import com.hashtagassignment.room.dao.HashTagDao
import com.hashtagassignment.room.entity.HashTag

class HashTagDatasource(var hashTagDao: HashTagDao) : IHashTagRepository {
    override fun getHashTag(): HashTag {
        return hashTagDao.getHashTag()
    }

    override fun saveData(vararg data: HashTag) {
        hashTagDao.insertHashTag(*data)
    }

    companion object {
        private var instance: HashTagDatasource? = null
        fun getInstance(hashTagDao: HashTagDao): HashTagDatasource {
            if (instance == null) {
                instance = HashTagDatasource(hashTagDao)
            }
            return instance as HashTagDatasource
        }

    }

}