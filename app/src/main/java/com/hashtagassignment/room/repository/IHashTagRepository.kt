package com.hashtagassignment.room.repository

import com.hashtagassignment.room.entity.HashTag

interface IHashTagRepository {
    fun getHashTag(): HashTag
    fun saveData(vararg data: HashTag)

}