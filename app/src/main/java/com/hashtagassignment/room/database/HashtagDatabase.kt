package com.hashtagassignment.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.hashtagassignment.room.dao.HashTagDao
import com.hashtagassignment.room.entity.HashTag
import com.hashtagassignment.room.typeconverter.Conveter
import dev.matrix.roomigrant.GenerateRoomMigrations

@Database(entities = arrayOf(HashTag::class), version = 1, exportSchema = false)
@TypeConverters(Conveter::class)
@GenerateRoomMigrations
abstract class HashtagDatabase : RoomDatabase() {
        abstract fun hashTagDao(): HashTagDao

        companion object {
            private var instance: HashtagDatabase? = null

            fun getInstance(context: Context): HashtagDatabase? {

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context,
                        HashtagDatabase::class.java,
                        "HashtagDatabase.sqlite"
                    ).allowMainThreadQueries(

                    ).build()
                }
                return instance
            }
        }
}