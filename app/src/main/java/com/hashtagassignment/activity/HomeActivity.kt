package com.hashtagassignment.activity

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.gson.JsonParser
import com.hashtagassignment.R
import com.hashtagassignment.adapter.BottomAdapter
import com.hashtagassignment.adapter.TopAdapter
import com.hashtagassignment.callbackinterface.OnItemClick
import com.hashtagassignment.databinding.ActivityHomeBinding
import com.hashtagassignment.room.entity.HashTag
import com.hashtagassignment.room.entity.RouteInfo
import com.hashtagassignment.room.entity.RouteTimings
import com.hashtagassignment.utils.Constant.checktimings
import com.hashtagassignment.utils.Constant.convertJsonToList
import com.hashtagassignment.utils.Constant.convertKeyValuePairToMap
import com.hashtagassignment.utils.Constant.getCurrentTime
import com.hashtagassignment.utils.Constant.getJsonDataFromAsset
import com.hashtagassignment.utils.Constant.hashtagRepository


class HomeActivity : AppCompatActivity(), OnItemClick {

    lateinit var binding: ActivityHomeBinding
    lateinit var topAdapter: TopAdapter
    lateinit var bottomAdapter: BottomAdapter

    lateinit var component_name: ComponentName
    private var jobScheduler: JobScheduler? = null
    private var jobInfo: JobInfo? = null

    var route: String? = null
    var pos: Int = 0
    val INTERVAL = 1000 * 60 * 1.toLong()
    var mHandler: Handler = Handler()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@HomeActivity, R.layout.activity_home)
        getDataAndStoreLocal()
    }

    fun getDataAndStoreLocal() {
        try {
            var hasTag = hashtagRepository.getHashTag()
            if (hasTag != null) {
                setDataToAdapter(hasTag)
            } else {
                var data = getJsonDataFromAsset(this@HomeActivity, "sample.json")
                var jsonParser = JsonParser()
                var jsonObject = jsonParser.parse(data).asJsonObject

                var routeTime =
                    convertKeyValuePairToMap(jsonObject.get("routeTimings").asJsonObject)
                var routeInfo = convertJsonToList(jsonObject.get("routeInfo").asJsonArray)

                var hashTag = HashTag()
                var routeTimings = RouteTimings()
                routeTimings.route = routeTime

                hashTag.routeTimings = routeTimings
                hashTag.routeInfo = routeInfo

                hashtagRepository.saveData(hashTag)
                getDataAndStoreLocal()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setDataToAdapter(hasTag: HashTag) {
        topAdapter = TopAdapter(this@HomeActivity, hasTag.routeInfo!!, this)
        binding.recycleOne.adapter = topAdapter
        topAdapter.notifyDataSetChanged()

        route = hasTag.routeInfo!![0].id
        setBottomAdapter(route)
    }

    private fun setBottomAdapter(route: String?) {
        var routeTimings = hashtagRepository.getHashTag().routeTimings!!.route!![route]
        routeTimings = routeTimings!!.filter { checktimings(it.tripStartTime!!, getCurrentTime()) }
        if (!routeTimings.isNullOrEmpty()) {
            bottomAdapter = BottomAdapter(this@HomeActivity, routeTimings!!, this)
            binding.recycleTwo.adapter = bottomAdapter
            bottomAdapter.notifyDataSetChanged()
            binding.nodataTv.visibility = View.GONE
            binding.recycleTwo.visibility = View.VISIBLE
        } else {
            binding.nodataTv.visibility = View.VISIBLE
            binding.recycleTwo.visibility = View.GONE
        }

        mHandler.postDelayed(mHandlerTask, INTERVAL)
    }

    override fun itemClick(p: Int, any: Any) {
        pos = p
        var topItem = any as RouteInfo
        setBottomAdapter(topItem.id)
    }

    fun updateUI() {
        route = hashtagRepository.getHashTag().routeInfo!![pos].id
        setBottomAdapter(route)
    }

    var mHandlerTask: Runnable = object : Runnable {
        override fun run() {
            updateUI()
        }
    }

    fun stopRepeatingTask() {
        mHandler.removeCallbacks(mHandlerTask)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopRepeatingTask()
    }
/*

    @SuppressLint("NewApi")
    fun startBackgroundTask() {
        jobScheduler =
            applicationContext.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        component_name = ComponentName(applicationContext, StartJobService::class.java!!)
        jobInfo = JobInfo.Builder(1, component_name)
            .setPeriodic(6 * 1000)
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
            .setRequiresCharging(false)
            .build()
        jobScheduler!!.schedule(jobInfo!!)

    }
*/


}
