package com.hashtagassignment.utils

import android.content.Context
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.hashtagassignment.room.database.HashtagDatabase
import com.hashtagassignment.room.entity.RouteInfo
import com.hashtagassignment.room.entity.RouteTimingSub
import com.hashtagassignment.room.repository.HashTagRepository
import org.json.JSONException
import java.io.IOException
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


object Constant {
    lateinit var hashtagDatabase: HashtagDatabase
    lateinit var hashtagRepository: HashTagRepository

    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    fun convertKeyValuePairToMap(json: JsonObject): Map<String, List<RouteTimingSub>> {
        var map = HashMap<String, List<RouteTimingSub>>()
        var gson = Gson()
        val iter: Set<String> = json.keySet()
        var iterator = iter.iterator()
        while (iterator.hasNext()) {
            val key = iterator.next()
            try {
                val value: Any = json.get(key)
                val type: Type = object : TypeToken<List<RouteTimingSub?>?>() {}.type
                val list: List<RouteTimingSub> = gson.fromJson(value.toString(), type)
                map.put(key, list)
            } catch (e: JSONException) { // Something went wrong!
            }
        }
        return map
    }

    fun convertJsonToList(json: JsonArray): List<RouteInfo>? {
        var gson = Gson()
        var list: List<RouteInfo>? = null
        try {
            val type: Type = object : TypeToken<List<RouteInfo>>() {}.type
            list = gson.fromJson(json.toString(), type)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return list
    }

    fun getCurrentTime(): String {
        val timeInMillis = System.currentTimeMillis()
        val cal1 = Calendar.getInstance()
        cal1.timeInMillis = timeInMillis
        val dateFormat = SimpleDateFormat("HH:mm")
        return dateFormat.format(cal1.time)
    }

     fun checktimings(time: String, endtime: String): Boolean {
        val pattern = "HH:mm"
        val sdf = SimpleDateFormat(pattern)
        try {
            val date1 = sdf.parse(time)
            val date2 = sdf.parse(endtime)
            return date1.after(date2)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return false
    }

}