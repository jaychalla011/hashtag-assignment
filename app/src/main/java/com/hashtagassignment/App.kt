package com.hashtagassignment

import android.app.Application
import com.hashtagassignment.room.database.HashtagDatabase
import com.hashtagassignment.room.repository.HashTagDatasource
import com.hashtagassignment.room.repository.HashTagRepository
import com.hashtagassignment.utils.Constant.hashtagDatabase
import com.hashtagassignment.utils.Constant.hashtagRepository

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        initDatabase()
    }

    fun initDatabase() {
        hashtagDatabase = HashtagDatabase.getInstance(this)!!
        hashtagRepository = HashTagRepository.getInstance(
            HashTagDatasource.getInstance(
                hashtagDatabase.hashTagDao()
            )
        )
    }
}