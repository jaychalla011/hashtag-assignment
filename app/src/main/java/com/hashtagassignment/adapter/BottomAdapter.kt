package com.hashtagassignment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.hashtagassignment.R
import com.hashtagassignment.callbackinterface.OnItemClick
import com.hashtagassignment.databinding.VerticalItemBinding
import com.hashtagassignment.room.entity.RouteInfo
import com.hashtagassignment.room.entity.RouteTimingSub
import com.hashtagassignment.room.entity.RouteTimings
import kotlinx.android.synthetic.main.vertical_item.view.*

class BottomAdapter(
    var context: Context,
    var list: List<RouteTimingSub>,
    var onItemClick: OnItemClick
) : RecyclerView.Adapter<BottomAdapter.ViewHolder>() {
    lateinit var binding: VerticalItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var layoutInflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.vertical_item, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.star_time_tv.text = "Start time : " + list[position].tripStartTime
        holder.itemView.avl_seat_tv.text =
            "Available Seat : " + list[position].avaiable + "/" + list[position].totalSeats
    }

    class ViewHolder(binding: VerticalItemBinding) : RecyclerView.ViewHolder(binding.root) {}
}