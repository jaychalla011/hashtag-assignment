package com.hashtagassignment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.hashtagassignment.R
import com.hashtagassignment.callbackinterface.OnItemClick
import com.hashtagassignment.databinding.HorizontalItemBinding
import com.hashtagassignment.room.entity.RouteInfo
import com.hashtagassignment.room.entity.RouteTimings
import kotlinx.android.synthetic.main.horizontal_item.view.*

class TopAdapter(
    var context: Context,
    var list: List<RouteInfo>,
    var onItemClick: OnItemClick
) : RecyclerView.Adapter<TopAdapter.ViewHolder>() {
    lateinit var binding: HorizontalItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopAdapter.ViewHolder {
        var layoutInflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.horizontal_item, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TopAdapter.ViewHolder, position: Int) {
        holder.itemView.route_tv.text = list[position].name
        holder.itemView.from_to_tv.text = list[position].source + " - " + list[position].destination
        holder.itemView.time_tv.text = list[position].tripDuration

        holder.itemView.setOnClickListener {
            onItemClick.itemClick(position, list[position])
        }
    }
    class ViewHolder(binding: HorizontalItemBinding) : RecyclerView.ViewHolder(binding.root) {}
}